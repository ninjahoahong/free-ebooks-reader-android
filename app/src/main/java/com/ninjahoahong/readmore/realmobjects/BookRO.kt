package com.ninjahoahong.readmore.realmobjects

import android.os.Parcelable
import com.ninjahoahong.readmore.reponses.Book
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
open class BookRO(
    @PrimaryKey
    open var bookId: String? = null,
    @Index
    open var title: String? = null,
    open var subTitle: String? = null
) : RealmObject(), Parcelable {

    companion object {
        fun createFromBook(book: Book): BookRO =
            BookRO(book.id, book.volumeInfo.title, book.volumeInfo.subTitle)
    }
}