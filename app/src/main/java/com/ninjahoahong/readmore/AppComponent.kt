package com.ninjahoahong.readmore

import com.ninjahoahong.readmore.books.BooksFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AppModule::class)
])
interface AppComponent {
    fun inject(booksFragment: BooksFragment)
}

