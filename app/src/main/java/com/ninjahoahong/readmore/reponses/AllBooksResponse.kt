package com.ninjahoahong.readmore.reponses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllBooksResponse(
    val items: List<Book>
) : Parcelable

