package com.ninjahoahong.readmore.reponses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VolumeInfo (
    val title: String,
    val subTitle: String
) : Parcelable

