package com.ninjahoahong.readmore.books

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.VERTICAL
import com.ninjahoahong.readmore.AndroidApp
import com.ninjahoahong.readmore.ApiService
import com.ninjahoahong.readmore.BaseFragment
import com.ninjahoahong.readmore.R
import com.ninjahoahong.readmore.clearIfNotDisposed
import com.ninjahoahong.readmore.hide
import com.ninjahoahong.readmore.onTextChanged
import com.ninjahoahong.readmore.realmobjects.BookRO
import com.ninjahoahong.readmore.schedulers.SchedulerProvider
import com.ninjahoahong.readmore.show
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.realm.Case
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_books.*
import kotlinx.android.synthetic.main.view_loading.*
import javax.inject.Inject

class BooksFragment : BaseFragment() {

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    private lateinit var realm: Realm
    private lateinit var booksAdapter: BooksAdapter
    private var indexOfContentView: Int = 0
    private var indexOfLoadingView: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidApp.appComponent.inject(this)
        indexOfContentView = viewAnimator.indexOfChild(contentView)
        indexOfLoadingView = viewAnimator.indexOfChild(loadingView)
        realm = Realm.getDefaultInstance()
        booksAdapter = BooksAdapter(realm.where<BookRO>().findAllAsync())
        searchView.onTextChanged { bookTitle ->
            val query = when {
                bookTitle.isNotEmpty() -> realm.where<BookRO>().contains("title", bookTitle, Case.INSENSITIVE)
                else -> realm.where<BookRO>()
            }
            booksAdapter.updateData(query.findAllAsync())
        }
        booksView.layoutManager = LinearLayoutManager(context, VERTICAL, false)
        booksView.adapter = booksAdapter
        loadBooks()
    }

    private fun loadBooks() {
        loadingView.show()
        compositeDisposable += apiService
            .getBooks(mapOf("q" to "a"))
            .retry()
            .subscribeOn(schedulerProvider.io())
            .doOnNext { books ->
                val bookROs = books.items.map { book -> BookRO.createFromBook(book) }

                Realm.getDefaultInstance().use { r ->
                    r.executeTransaction { realm ->
                        realm.insertOrUpdate(bookROs)
                    }
                }
            }
            .observeOn(schedulerProvider.ui())
            .doFinally { loadingView.hide() }
            .subscribeBy(
                onNext = {
                    println("Saved books to Realm.")
                },
                onError = {
                    it.printStackTrace()
                },
                onComplete = {
                }
            )
    }

    override fun onDestroyView() {
        compositeDisposable.clearIfNotDisposed()
        super.onDestroyView()
        realm.close()
    }
}