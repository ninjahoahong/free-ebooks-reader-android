package com.ninjahoahong.readmore.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ninjahoahong.readmore.BaseFragment
import com.ninjahoahong.readmore.R
import com.ninjahoahong.readmore.realmobjects.BookRO
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_book_detail.*


class BookDetailFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_book_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Realm.getDefaultInstance().use { r ->
            r.executeTransaction { realm ->
                val bookRO = realm.where(BookRO::class.java).equalTo("bookId", getKey<BookDetailKey>().bookId).findFirst()
                bookTitle.text = bookRO?.title ?: ""
                bookSubtitle.text = bookRO?.subTitle ?: ""
            }
        }
    }
}
