package com.ninjahoahong.readmore.books

import com.ninjahoahong.readmore.BaseKey
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookDetailKey(
    val tag: String,
    val bookId: String
) : BaseKey() {

    constructor(bookId: String) : this("BookDetailKey", bookId)

    override fun createFragment() = BookDetailFragment()
}