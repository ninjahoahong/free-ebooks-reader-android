package com.ninjahoahong.readmore.books

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ninjahoahong.readmore.MainActivity
import com.ninjahoahong.readmore.R
import com.ninjahoahong.readmore.onClick
import com.ninjahoahong.readmore.realmobjects.BookRO
import io.realm.RealmRecyclerViewAdapter
import io.realm.RealmResults
import kotlinx.android.synthetic.main.view_item.view.*

class BooksAdapter(
    items: RealmResults<BookRO>
) : RealmRecyclerViewAdapter<BookRO, BooksAdapter.BooksViewHolder>(items, true) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder =
        BooksViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.view_item, parent, false))

    override fun onBindViewHolder(booksViewHolder: BooksViewHolder, position: Int) {
        val item = data?.get(position)
        booksViewHolder.bind(item!!)
        booksViewHolder.onClick {
            MainActivity[(booksViewHolder.itemView.context)]
                .navigateTo(BookDetailKey(item.bookId!!))
        }
    }

    public class BooksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val bookTitle: TextView = itemView.bookTitle;

        fun bind(book: BookRO) {
            bookTitle.text = book.title
        }

        fun onClick(click: (View?) -> Unit) {
            itemView.onClick(click)
        }
    }
}