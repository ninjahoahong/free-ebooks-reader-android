package com.ninjahoahong.readmore.books

import com.ninjahoahong.readmore.BaseKey
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BooksKey(val tag: String) : BaseKey() {
    constructor() : this("BooksKey")

    override fun createFragment() = BooksFragment()
}