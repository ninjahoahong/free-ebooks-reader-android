package com.ninjahoahong.readmore

import com.ninjahoahong.readmore.reponses.AllBooksResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {
    @GET("volumes")
    fun getBooks(@QueryMap options: Map<String, String>): Observable<AllBooksResponse>
}