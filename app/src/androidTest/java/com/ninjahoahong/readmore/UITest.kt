package com.ninjahoahong.readmore

import android.support.test.espresso.action.ViewActions.click
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.ninjahoahong.readmore.R.id.bookSubtitle
import com.ninjahoahong.readmore.R.id.bookTitle
import com.ninjahoahong.readmore.R.id.booksView
import com.ninjahoahong.readmore.R.id.loadBooksButton
import com.ninjahoahong.readmore.books.BooksAdapter
import com.zhuinden.espressohelper.checkCurrentActivityIs
import com.zhuinden.espressohelper.checkHasText
import com.zhuinden.espressohelper.checkIsVisible
import com.zhuinden.espressohelper.performActionOnRecyclerItemAtPosition
import com.zhuinden.espressohelper.performClick
import com.zhuinden.espressohelper.rotateOrientation
import com.zhuinden.espressohelper.waitOnMainThread
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UITest {

    @JvmField
    @field:Rule
    var rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun goThroughScreens() {
        val activity = rule.activity
        checkCurrentActivityIs<MainActivity>()
        activity.rotateOrientation()
        loadBooksButton.checkIsVisible()
        activity.rotateOrientation()
        loadBooksButton.checkHasText("load books")
        loadBooksButton.performClick()

        booksView.performActionOnRecyclerItemAtPosition<BooksAdapter.BooksViewHolder>(0, click())

        bookTitle.checkIsVisible()
        bookSubtitle.checkIsVisible()

        rule.waitOnMainThread { callback ->
            // wait fomr something
            callback()
        }
    }
}